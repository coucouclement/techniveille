(function($){  
    $.fn.fluidScrollTo = function(speed){  
        if(!speed) var speed = 800;  
   
        return this.each(function(){  
            $(this).click(function(){  
                var goscroll = false;  
                var the_hash = $(this).attr("href");  
                var regex = new RegExp("(.*)\#(.*)","gi");  
                var the_element = '';  
   
                if(the_hash.match("\#(.+)")) {  
   
                    the_hash = the_hash.replace(regex,"$2");  
   
                    if($("#"+the_hash).length>0) {  
                        the_element = "#" + the_hash;  
                        goscroll = true;  
                    }  
                    else if($("a[name=" + the_hash + "]").length>0) {  
                        the_element = "a[name=" + the_hash + "]";  
                        goscroll = true;  
                    }  
   
                    if(goscroll) {  
                        var container = 'html';
                        $(container).animate({  
                            scrollTop:$(the_element).offset().top  
                        }, {duration: speed, easing: 'easeInOutQuint'});  
                        return false;  
                    }  
                }  
            });  
        });  
    };  
})(jQuery);

$(document).ready(function(){
    var $body = $('body'),
        $slides = $body.find('.layout'),
        $slidesNav = $body.find('.slide-timeline'),
        offsets = [],
        darkness = [],
        scrollTop = 0,
        currentSlide;
    $.each($slides, function(e){
        offsets.push(($(this).offset().top-1));
        darkness.push($(this).data('darkness'));
    });
    $(window).on('scroll', function(e){
        scrollTop = e.currentTarget.scrollY;
        for(i=0,j=offsets.length;i<j;i++){
            if(scrollTop > offsets[i] && scrollTop < offsets[i+1]){
                currentSlide = i;
                if(!darkness[i]){
                    if(!($('.nav-right__menu').hasClass('nav-right__menu--dark'))){
                        $('.nav-right__menu').addClass('nav-right__menu--dark');
                        $('.timeline').addClass('dark');
                    }
                } else if($('.nav-right__menu').hasClass('nav-right__menu--dark')){
                    $('.nav-right__menu').removeClass('nav-right__menu--dark');
                    $('.timeline').removeClass('dark');
                }
                $.each($slidesNav, function(e) {
                    if ($(this).data('slide') < currentSlide) {
                        $(this).addClass('show');
                        $(this).removeClass('slide-current');
                    } else if ($(this).data('slide') === currentSlide) {
                        $(this).addClass('show slide-current');
                    } else {
                        $(this).removeClass('show slide-current');
                    }
                });
            }
        }
    });

    // Overlay (à refactoriser)
    $('.js-toggle-overlay').on('click', function(e){
        e.preventDefault();
        if(!($(this).hasClass('active'))){
            $(this).addClass('active');
            $('.overlay--about').removeClass('hidden');
        }
        else {
            $(this).removeClass('active');
            $('.overlay--about').addClass('hidden');
        }
    });

    var scrollingScreen = false;
    var top = 0;
    $('body').mousewheel(function(event, delta) {
        if ( !scrollingScreen ) {
            scrollingScreen = true;

            var candidates = $('.layout').filter(function() {
                if ( delta < 0 )
                    return $(this).offset().top > top + 1;
                else
                    return $(this).offset().top < top - 1;
            });

            if ( candidates.length > 0 ) {
                if ( delta < 0 )
                    top = candidates.first().offset().top;
                else if ( delta > 0 )
                    top = candidates.last().offset().top;
            }

            $('html,body').animate({ scrollTop:top }, {duration: 800, easing: 'easeInOutQuint', complete: function() {
                scrollingScreen = false;
            }});
        }
        return false;
    });

    $(window).keydown(function(event){  
        if ( !scrollingScreen ) {  
            var key = event.which;             
            if (key === 38) { //key up
                scrollingScreen = true;
                var candidates = $('.layout').filter(function() {
                    return $(this).offset().top < top - 1;
                });
                if ( candidates.length > 0 ) {
                    top = candidates.last().offset().top;
                }
                $('html,body').animate({ scrollTop:top }, {duration: 800, easing: 'easeInOutQuint', complete: function() {
                    scrollingScreen = false;
                }});
                return false;
            } else if (key === 40) { //key down
                scrollingScreen = true;
                var candidates = $('.layout').filter(function() {
                    return $(this).offset().top > top + 1;
                });
                if ( candidates.length > 0 ) {
                    top = candidates.first().offset().top;
                }
                $('html,body').animate({ scrollTop:top }, {duration: 800, easing: 'easeInOutQuint', complete: function() {
                    scrollingScreen = false;
                }});
                return false;
            }
        }
      });
    $('a[href^="#"]').fluidScrollTo();
});
